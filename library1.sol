//SPDX-License-Identifyer: MIT

pragma solidity 0.8.9;

contract Owner {
    address owner;
    
    constructor() public{
        owner = msg.sender;
    }
    
    modifier onlyOwner{
        require(msg.sender == owner, "You are not allowed");
        _;
    }
}